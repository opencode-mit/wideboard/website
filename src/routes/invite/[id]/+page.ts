import type { PageLoad } from './$types';

export const load = (async ({ params }) => {
	return {
		courseName: 'Linux Foundations',
        role: 'student',
		id: params.id
	};
}) satisfies PageLoad;
