import { redirect } from '@sveltejs/kit';
import type { PageLoad } from './$types';
import { courses } from '$lib';

export const load = (async () => {
	redirect(300, '/class/' + courses.at(0)?.id);
}) satisfies PageLoad;