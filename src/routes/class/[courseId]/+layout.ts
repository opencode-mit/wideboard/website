import type { LayoutLoad } from './$types';

export const load = (async ({ params }) => {
	return {
		courseId: parseInt(params.courseId),
		threadId: params.threadId ? parseInt(params.threadId) : undefined
	};
}) satisfies LayoutLoad;
