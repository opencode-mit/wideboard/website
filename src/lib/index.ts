import type { BasicClass } from './types';

export function formatDateShort(date: Date): string {
	const yearFormat = new Intl.DateTimeFormat(undefined, {
		year: 'numeric',
		month: 'numeric',
		day: 'numeric'
	});

	const monthFormat = new Intl.DateTimeFormat(undefined, {
		month: 'long',
		day: '2-digit'
	});

	const timeFormat = new Intl.DateTimeFormat(undefined, {
		hour: '2-digit',
		minute: '2-digit'
	});

	const today = new Date();

	if (today.getFullYear() != date.getFullYear()) return yearFormat.format(date);
	if (today.getMonth() != date.getMonth() || today.getDate() != date.getDate())
		return monthFormat.format(date);
	return timeFormat.format(date);
}

export function formatDateLong(date: Date): string {
	return date.toLocaleString(undefined, {
		month: 'long',
		day: 'numeric',
		year: 'numeric',
		hour: 'numeric',
		minute: 'numeric'
	});
}

export const courses: BasicClass[] = [
	{
		id: 0,
		title: '6.101 Fundementals in Computer Science',
		subscribers: new Map(),
		permissions: {
			instructor: {
				canDeleteThreads: true
			},
			student: {
				canDeleteThreads: false
			}
		},
		tags: ['question', 'note'],
		threads: []
	},
	{
		id: 1,
		title: 'LMT',
		subscribers: new Map(),
		permissions: {
			instructor: {
				canDeleteThreads: true
			},
			student: {
				canDeleteThreads: false
			}
		},
		tags: ['question', 'note'],
		threads: [
			{
				id: 1,
				date: new Date(),
				isInstructor: false,
				subject: 'How to login into MIT Secure?',
				replies: [
					{
						id: 0,
						author: {
							id: 2,
							name: 'Abutalib Namazov'
						},
						liked: 2,
						date: new Date(),
						replies: [],
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
					}
				],
				requiresAnswer: true,
				unread: 2,
				resolved: true,
				tags: ['question']
			},
			{
				id: 3,
				date: new Date(),
				isInstructor: false,
				subject: 'How to login into MIT Secure?',
				replies: [
					{
						id: 0,
						author: {
							id: 2,
							name: 'Abutalib Namazov'
						},
						liked: 2,
						date: new Date(),
						replies: [],
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
					}
				],
				requiresAnswer: true,
				unread: 2,
				resolved: true,
				tags: ['question']
			},
			{
				id: 4,
				date: new Date(),
				isInstructor: false,
				subject: 'How to login into MIT Secure?',
				replies: [
					{
						id: 0,
						author: {
							id: 2,
							name: 'Abutalib Namazov'
						},
						liked: 2,
						date: new Date(),
						replies: [],
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
					}
				],
				requiresAnswer: true,
				unread: 2,
				resolved: true,
				tags: ['question']
			},
			{
				id: 5,
				date: new Date(),
				subject: 'How to login into MIT Secure?',
				isInstructor: false,
				replies: [
					{
						id: 0,
						author: {
							id: 2,
							name: 'Abutalib Namazov'
						},
						liked: 2,
						date: new Date(),
						replies: [],
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
					}
				],
				requiresAnswer: true,
				unread: 2,
				resolved: true,
				tags: ['question']
			},
			{
				id: 6,
				date: new Date(),
				subject: 'How to login into MIT Secure?',
				isInstructor: false,
				replies: [
					{
						id: 0,
						author: {
							id: 2,
							name: 'Abutalib Namazov'
						},
						liked: 2,
						date: new Date(),
						replies: [],
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
					}
				],
				requiresAnswer: true,
				unread: 2,
				resolved: true,
				tags: ['question']
			},
			{
				id: 7,
				date: new Date(),
				isInstructor: false,
				subject: 'How to login into MIT Secure?',
				replies: [
					{
						id: 0,
						author: {
							id: 2,
							name: 'Abutalib Namazov'
						},
						liked: 2,
						date: new Date(),
						replies: [],
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
					}
				],
				requiresAnswer: true,
				unread: 2,
				resolved: true,
				tags: ['question']
			},
			{
				id: 8,
				date: new Date(),
				isInstructor: false,
				subject: 'How to login into MIT Secure?',
				replies: [
					{
						id: 0,
						author: {
							id: 2,
							name: 'Abutalib Namazov'
						},
						liked: 2,
						date: new Date(),
						replies: [],
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
					}
				],
				requiresAnswer: true,
				unread: 2,
				resolved: true,
				tags: ['question']
			},
			{
				id: 9,
				date: new Date(),
				subject: 'How to login into MIT Secure?',
				isInstructor: true,
				replies: [
					{
						id: 0,
						author: {
							id: 2,
							name: 'Abutalib Namazov'
						},
						liked: 2,
						date: new Date(),
						replies: [],
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
					}
				],
				requiresAnswer: true,
				unread: 2,
				resolved: true,
				tags: ['question']
			},
			{
				id: 10,
				date: new Date(),
				subject: 'How to login into MIT Secure?',
				isInstructor: false,
				replies: [
					{
						id: 0,
						author: {
							id: 2,
							name: 'Abutalib Namazov'
						},
						liked: 2,
						date: new Date(),
						replies: [],
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
					}
				],
				requiresAnswer: true,
				unread: 2,
				resolved: true,
				tags: ['question']
			},
			{
				id: 2,
				date: new Date(Date.now() - 1000 * 60 * 60 * 24 * 5),
				isInstructor: false,
				subject: 'How to gradute? How to gradute? How to gradute? How to gradute? How to gradute?',
				replies: [
					{
						id: 1,
						content:
							'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.',
						author: {
							id: 1,
							name: 'Khaleel Al-Adhami'
						},
						liked: 3,
						date: new Date(),
						replies: [
							{
								id: 2,
								author: {
									id: 1,
									name: 'Khaleel Al-Adhami'
								},
								liked: 3,
								date: new Date(),
								replies: [],
								content:
									'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
							},
							{
								id: 3,
								author: {
									id: 1,
									name: 'Khaleel Al-Adhami'
								},
								liked: 3,
								date: new Date(),
								replies: [
									{
										id: 4,
										author: {
											id: 1,
											name: 'Khaleel Al-Adhami'
										},
										liked: 3,
										date: new Date(),
										replies: [],
										content:
											'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
									}
								],
								content:
									'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim aeque doleamus animo, cum corpore dolemus, fieri tamen permagna accessio potest, si aliquod aeternum et infinitum impendere malum nobis opinemur. Quod idem licet transferre in voluptatem, ut postea variari voluptas distinguique possit, augeri amplificarique non possit. At etiam Athenis, ut e patre audiebam.'
							}
						]
					}
				],
				requiresAnswer: false,
				unread: 0,
				resolved: false,
				tags: ['note']
			}
		]
	}
];
