type UserId = number;
type ClassId = number;
type ThreadId = number;
type LetterId = number;

export type User = {
	id: UserId;
};

type ElementType<T extends ReadonlyArray<unknown>> =
	T extends ReadonlyArray<infer ElementType> ? ElementType : never;

export type Class<R extends ReadonlyArray<string>> = {
	id: ClassId;
	subscribers: Map<User, ElementType<R>>;
	title: string;
	permissions: {
		[P in ElementType<R>]: Permissions;
	};
	tags: string[];
	threads: Thread[];
};

export type Permissions = {
	canDeleteThreads: boolean;
};

export const BasicRoles = ['instructor', 'student'] as const;

export type BasicClass = Class<typeof BasicRoles>;

export type Thread = {
	id: ThreadId;
	date: Date;
	subject: string;
	isInstructor: boolean;
	replies: Reply[];
	unread: number;
	requiresAnswer: boolean;
	tags: string[];
	resolved: boolean;
};

export type Reply = {
	id: LetterId;
	author: Author;
	date: Date;
	content: string;
	liked: number;
	replies: Reply[];
};

export type Author = {
	id: UserId;
	name: string;
};
